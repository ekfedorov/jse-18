package ru.ekfedorov.tm.service;

import ru.ekfedorov.tm.api.repository.IProjectRepository;
import ru.ekfedorov.tm.api.service.IProjectService;
import ru.ekfedorov.tm.enumerated.Status;
import ru.ekfedorov.tm.exception.incorrect.IncorrectIndexException;
import ru.ekfedorov.tm.exception.empty.DescriptionIsEmptyException;
import ru.ekfedorov.tm.exception.empty.IdIsEmptyException;
import ru.ekfedorov.tm.exception.system.NullComparatorException;
import ru.ekfedorov.tm.exception.empty.NameIsEmptyException;
import ru.ekfedorov.tm.exception.system.NullProjectException;
import ru.ekfedorov.tm.model.Project;

import java.util.Comparator;
import java.util.List;

import static ru.ekfedorov.tm.util.ValidateUtil.*;

public class ProjectService implements IProjectService {

    private final IProjectRepository projectRepository;

    public ProjectService(final IProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    @Override
    public List<Project> findAll(final Comparator<Project> comparator) throws Exception {
        if (comparator == null) throw new NullComparatorException();
        return projectRepository.findAll(comparator);
    }

    @Override
    public Project add(final String name, final String description) throws Exception {
        if (isEmpty(name)) throw new NameIsEmptyException();
        if (isEmpty(description)) throw new DescriptionIsEmptyException();
        final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        projectRepository.add(project);
        return project;
    }

    @Override
    public void add(final Project project) throws Exception {
        if (project == null) throw new NullProjectException();
        projectRepository.add(project);
    }

    @Override
    public void remove(final Project project) throws Exception {
        if (project == null) throw new NullProjectException();
        projectRepository.remove(project);
    }

    @Override
    public void clear() {
        projectRepository.clear();
    }

    @Override
    public Project findOneByIndex(final Integer index) throws Exception {
        if (notNullOrLessZero(index)) throw new IncorrectIndexException(index);
        return projectRepository.findOneByIndex(index);
    }

    @Override
    public Project findOneById(final String id) throws Exception {
        if (isEmpty(id)) throw new IdIsEmptyException();
        return projectRepository.findOneById(id);
    }

    @Override
    public Project findOneByName(final String name) throws Exception {
        if (isEmpty(name)) throw new NameIsEmptyException();
        return projectRepository.findOneByName(name);
    }

    @Override
    public Project removeOneByIndex(final Integer index) throws Exception {
        if (notNullOrLessZero(index)) throw new IncorrectIndexException(index);
        return projectRepository.removeOneByIndex(index);
    }

    @Override
    public Project removeOneByName(final String name) throws Exception {
        if (isEmpty(name)) throw new NameIsEmptyException();
        return projectRepository.removeOneByName(name);
    }

    @Override
    public Project removeOneById(final String id) throws Exception {
        if (isEmpty(id)) throw new IdIsEmptyException();
        return projectRepository.removeOneById(id);
    }

    @Override
    public Project updateProjectByIndex(
            final Integer index, final String name, final String description
    ) throws Exception {
        if (notNullOrLessZero(index)) throw new IncorrectIndexException(index);
        if (isEmpty(name)) throw new NameIsEmptyException();
        final Project project = findOneByIndex(index);
        if (project == null) throw new NullProjectException();
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project updateProjectById(
            final String id, final String name, final String description
    ) throws Exception {
        if (isEmpty(id)) throw new IdIsEmptyException();
        if (isEmpty(name)) throw new NameIsEmptyException();
        final Project project = findOneById(id);
        if (project == null) throw new NullProjectException();
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project startProjectById(final String id) throws Exception {
        if (isEmpty(id)) throw new IdIsEmptyException();
        final Project project = findOneById(id);
        if (project == null) throw new NullProjectException();
        project.setStatus(Status.IN_PROGRESS);
        return project;
    }

    @Override
    public Project startProjectByIndex(final Integer index) throws Exception {
        if (notNullOrLessZero(index)) throw new IncorrectIndexException(index);
        final Project project = findOneByIndex(index);
        if (project == null) throw new NullProjectException();
        project.setStatus(Status.IN_PROGRESS);
        return project;
    }

    @Override
    public Project startProjectByName(final String name) throws Exception {
        if (isEmpty(name)) throw new NameIsEmptyException();
        final Project project = findOneByName(name);
        if (project == null) throw new NullProjectException();
        project.setStatus(Status.IN_PROGRESS);
        return project;
    }

    @Override
    public Project finishProjectById(final String id) throws Exception {
        if (isEmpty(id)) throw new IdIsEmptyException();
        final Project project = findOneById(id);
        if (project == null) throw new NullProjectException();
        project.setStatus(Status.COMPLETE);
        return project;
    }

    @Override
    public Project finishProjectByIndex(final Integer index) throws Exception {
        if (notNullOrLessZero(index)) throw new IncorrectIndexException(index);
        final Project project = findOneByIndex(index);
        if (project == null) throw new NullProjectException();
        project.setStatus(Status.COMPLETE);
        return project;
    }

    @Override
    public Project finishProjectByName(final String name) throws Exception {
        if (isEmpty(name)) throw new NameIsEmptyException();
        final Project project = findOneByName(name);
        if (project == null) throw new NullProjectException();
        project.setStatus(Status.COMPLETE);
        return project;
    }

    @Override
    public Project changeProjectStatusById(
            final String id, final Status status
    ) throws Exception {
        if (isEmpty(id)) throw new IdIsEmptyException();
        final Project project = findOneById(id);
        if (project == null) throw new NullProjectException();
        project.setStatus(status);
        return project;
    }

    @Override
    public Project changeProjectStatusByIndex(
            final Integer index, final Status status
    ) throws Exception {
        if (notNullOrLessZero(index)) throw new IncorrectIndexException(index);
        final Project project = findOneByIndex(index);
        if (project == null) throw new NullProjectException();
        project.setStatus(status);
        return project;
    }

    @Override
    public Project changeProjectStatusByName(
            final String name, final Status status
    ) throws Exception {
        if (isEmpty(name)) throw new NameIsEmptyException();
        final Project project = findOneByName(name);
        if (project == null) throw new NullProjectException();
        project.setStatus(status);
        return project;
    }

}
