package ru.ekfedorov.tm.service;

import ru.ekfedorov.tm.api.repository.IUserRepository;
import ru.ekfedorov.tm.api.service.IUserService;
import ru.ekfedorov.tm.model.User;
import ru.ekfedorov.tm.enumerated.Role;
import ru.ekfedorov.tm.exception.system.LoginExistException;
import ru.ekfedorov.tm.exception.empty.*;
import ru.ekfedorov.tm.util.HashUtil;

import java.util.List;

import static ru.ekfedorov.tm.util.ValidateUtil.isEmpty;

public class UserService implements IUserService {

    private final IUserRepository userRepository;

    public UserService(IUserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public List<User> findAll() {
        return userRepository.findAll();
    }

    @Override
    public User findById(final String id) throws Exception {
        if (isEmpty(id)) throw new IdIsEmptyException();
        return userRepository.findById(id);
    }

    @Override
    public User findByLogin(final String login) throws Exception {
        if (isEmpty(login)) throw new LoginIsEmptyException();
        return userRepository.findByLogin(login);
    }


    public User removeUser(final User user) {
        if (user == null) return null;
        return userRepository.removeUser(user);
    }

    @Override
    public User removeById(final String id) throws Exception {
        if (isEmpty(id)) throw new IdIsEmptyException();
        return userRepository.removeById(id);
    }

    @Override
    public User removeByLogin(final String login) throws Exception {
        if (isEmpty(login)) throw new LoginIsEmptyException();
        return userRepository.removeByLogin(login);
    }

    @Override
    public User create(final String login, final String password) throws Exception {
        if (isEmpty(login)) throw new LoginIsEmptyException();
        if (isLoginExist(login)) throw new LoginExistException();
        if (isEmpty(password)) throw new PasswordIsEmptyException();
        final User user = new User();
        user.setRole(Role.USER);
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(password));
        return userRepository.add(user);
    }

    @Override
    public User create(final String login, final String password, final String email) throws Exception {
        if (isEmpty(login)) throw new LoginIsEmptyException();
        if (isLoginExist(login)) throw new LoginExistException();
        if (isEmpty(password)) throw new PasswordIsEmptyException();
        if (isEmpty(email)) throw new EmailIsEmptyException();
        final User user = new User();
        user.setRole(Role.USER);
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(password));
        user.setEmail(email);
        return userRepository.add(user);
    }

    @Override
    public User create(final String login, final String password, final Role role) throws Exception {
        if (isEmpty(login)) throw new LoginIsEmptyException();
        if (isLoginExist(login)) throw new LoginExistException();
        if (isEmpty(password)) throw new PasswordIsEmptyException();
        if (role == null) throw new RoleIsEmptyException();
        final User user = new User();
        user.setRole(Role.USER);
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(password));
        user.setRole(role);
        return userRepository.add(user);
    }

    @Override
    public User setPassword(final String userId, final String password) throws Exception {
        if (isEmpty(userId)) throw new IdIsEmptyException();
        if (isEmpty(password)) throw new PasswordIsEmptyException();
        final User user = findById(userId);
        if (user == null) return null;
        final String hash = HashUtil.salt(password);
        if (hash == null) return null;
        user.setPasswordHash(hash);
        return user;
    }

    @Override
    public User userUpdate(
            final String userId,
            final String firstName,
            final String lastName,
            final String middleName
    ) throws Exception {
        if (isEmpty(userId)) throw new IdIsEmptyException();
        final User user = findById(userId);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        return user;
    }

    @Override
    public boolean isLoginExist(final String login) throws Exception {
        if (isEmpty(login)) throw new LoginIsEmptyException();
        return userRepository.isLoginExist(login);
    }

}
