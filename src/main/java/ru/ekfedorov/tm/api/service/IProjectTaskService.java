package ru.ekfedorov.tm.api.service;

import ru.ekfedorov.tm.model.Project;
import ru.ekfedorov.tm.model.Task;

import java.util.List;

public interface IProjectTaskService {

    List<Task> findAllByProjectId(String projectId) throws Exception;

    Task bindTaskByProject(String projectId, String taskId) throws Exception;

    Task unbindTaskFromProject(String taskId) throws Exception;

    Project removeProjectById(String projectId) throws Exception;

}
