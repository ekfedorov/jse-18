package ru.ekfedorov.tm.api.service;

import ru.ekfedorov.tm.command.AbstractCommand;

import java.util.Collection;

public interface ICommandService {

    AbstractCommand getCommandByName(String name);

    AbstractCommand getCommandByArg(String arg);

    Collection<AbstractCommand> getCommands();

    Collection<AbstractCommand> getArguments();

    Collection<String> getListCommandName();

    Collection<String> getListArgumentName();

    void add(AbstractCommand command);

}
