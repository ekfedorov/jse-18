package ru.ekfedorov.tm.api.service;

import ru.ekfedorov.tm.model.User;
import ru.ekfedorov.tm.enumerated.Role;

import java.util.List;

public interface IUserService {

    List<User> findAll();

    User findById(String id) throws Exception;

    User findByLogin(String login) throws Exception;

    User removeById(String id) throws Exception;

    User removeByLogin(String login) throws Exception;

    User create(String login, String password) throws Exception;

    User create(String login, String password, String email) throws Exception;

    User create(String login, String password, Role role) throws Exception;

    User setPassword(String userId, String password) throws Exception;

    User userUpdate(
            String userId,
            String firstName,
            String lastName,
            String middleName
    ) throws Exception;

    boolean isLoginExist(String login) throws Exception;

}
