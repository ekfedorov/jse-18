package ru.ekfedorov.tm.api.service;

public interface ServiceLocator {

    ICommandService getCommandService();

    IProjectService getProjectService();

    ITaskService getTaskService();

    IProjectTaskService getProjectTaskService();

    IUserService getUserService();

    IAuthService getAuthService();

}
