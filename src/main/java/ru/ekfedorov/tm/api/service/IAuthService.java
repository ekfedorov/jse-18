package ru.ekfedorov.tm.api.service;

import ru.ekfedorov.tm.model.User;

public interface IAuthService {

    IUserService getUserService();

    String getUserId() throws Exception;

    boolean isAuth();

    void logout();

    void login(String login, String password) throws Exception;

    void registry(String login, String password, String email) throws Exception;

    User getUser() throws Exception;

}
