package ru.ekfedorov.tm.model;

import java.util.UUID;

public abstract class AbstractEntity {

    protected String id = UUID.randomUUID().toString();

    public String getId() {
        return id;
    }

}

