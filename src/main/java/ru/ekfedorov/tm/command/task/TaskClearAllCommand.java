package ru.ekfedorov.tm.command.task;

import ru.ekfedorov.tm.command.AbstractTaskCommand;

public class TaskClearAllCommand extends AbstractTaskCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "task-clear";
    }

    @Override
    public String description() {
        return "Clear all task.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[CLEAR]");
        serviceLocator.getTaskService().clear();
        System.out.println("--- successfully cleared ---\n");
    }

}
