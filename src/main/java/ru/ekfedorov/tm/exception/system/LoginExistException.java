package ru.ekfedorov.tm.exception.system;

import ru.ekfedorov.tm.exception.AbstractException;

public class LoginExistException extends AbstractException {

    public LoginExistException() throws Exception {
        super("Warning! Login already exists...");
    }

}
