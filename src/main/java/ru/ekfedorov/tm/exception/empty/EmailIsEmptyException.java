package ru.ekfedorov.tm.exception.empty;

import ru.ekfedorov.tm.exception.AbstractException;

public class EmailIsEmptyException extends AbstractException {

    public EmailIsEmptyException() throws Exception {
        super("Error! Email is empty...");
    }

}
