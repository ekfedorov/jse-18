package ru.ekfedorov.tm.exception.empty;

import ru.ekfedorov.tm.exception.AbstractException;

public class PasswordIsEmptyException extends AbstractException {

    public PasswordIsEmptyException() throws Exception {
        super("Error! Password is empty...");
    }

}
