package ru.ekfedorov.tm.exception.empty;

import ru.ekfedorov.tm.exception.AbstractException;

public class IdIsEmptyException extends AbstractException {

    public IdIsEmptyException() throws Exception {
        super("Error! Id is empty...");
    }

}
