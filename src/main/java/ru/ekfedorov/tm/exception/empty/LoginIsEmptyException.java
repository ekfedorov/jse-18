package ru.ekfedorov.tm.exception.empty;

import ru.ekfedorov.tm.exception.AbstractException;

public class LoginIsEmptyException extends AbstractException {

    public LoginIsEmptyException() throws Exception {
        super("Error! Login is empty...");
    }

}

